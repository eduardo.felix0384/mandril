Mandril = (function() {
    function init() {
        setTimeout(function() {
            $('.close').bind('click', function(e) {
                e.preventDefault();
                $(this).parent().fadeOut('slow');
            });  
        }, 1000);
    }

    function showMessage(type, title, message) {
        $('#' + type + '-alert').removeClass('hide');
        $('#' + type + '-alert').fadeIn('slow');
        
        if(title) {
            $('#' + type + '-title').html(title);
        }
            
        if(message) {
            $('#' + type + '-message').html(message);
        }
    }

    function showSuccessMessage(title, message) {
        showMessage('success', title, message);
    }

    function showInfoMessage(title, message) {
        showMessage('info', title, message);
    }

    function showWarningMessage(title, message) {
        showMessage('warning', title, message);
    }

    function showDangerMessage(title, message) {
        showMessage('danger', title, message);
    }

    function serviceUrl (area, action) {
        return baseUrl + '/' + area + '/' + action + '/';
    }

    function callService (url, method, data, callback) {
        if (!data) {
            data = { };
        }

        $.ajax({
            dataType: "json",
            url: url,
            data: data,
            method: method
        }).done(function (response) {
            callback(response);
        });
    };

    function serviceGet(area, action, data, callback) {
        callService(serviceUrl(area, action), 'GET', data, callback);
    }

    function servicePost(area, action, data, callback) {
        callService(serviceUrl(area, action), 'POST', data, callback);
    }

    function template (_template, data, target) {
        var source = $(_template).html();
        var build = Handlebars.compile(source);
        $(target).html(build(data));
    }

    function setAjaxSubmit (form, validationRules, validationMessages, callback, errorCallback) {
        $(form).validate({
            rules: validationRules,
            messages: validationMessages,
            submitHandler: function(_form) {
                $('button[type=submit]', form).attr('disabled', 'disabled');

                $(_form).ajaxSubmit(function (data) {
                    if (data.error) {
                        if (errorCallback) {
                            errorCallback(data);
                        }
                    } else {
                        if (callback) {
                            callback(data);
                        } else {
                            showSuccessMessage('', 'Informações enviadas com sucesso');
                        }
                    }

                    $('button[type=submit]', form).removeAttr('disabled');
                });
            }
        });
    }

    function fillForm (data) {
        if (data.error) {
            showErrors(data.error);
        } else {
            var fields = Object.keys(data);

            for (var i = 0; i < fields.length; i++) {
                var field = fields[i];
                var inputType = $('input[name=' + field + ']').attr('type');

                if (!inputType)
                    inputType = $('input[data-field=' + field + ']').attr('type');

                if (!inputType)
                {
                    if ($('textarea[name=' + field + ']'))
                        inputType = 'textarea';
                }

                if (!inputType) {
                    if ($('select[name=' + field + ']'))
                        inputType = 'select';
                }

                if (inputType && inputType != 'radio' && inputType != 'checkbox') {
                    var value = $(data).attr(field);
                    $('input[name=' + field + ']').val(value);
                    $('input[data-field=' + field + ']').val(value);
                    $('textarea[name=' + field + ']').val(value);
                    $('select[name=' + field + ']').val(value);
                    $('select[data-field=' + field + ']').val(value);
                }
            }

            $('input[type=radio]').each(function () {
                var field = $(this).attr('name');
                var value = $(data).attr(field);

                if (field && value) {
                    if ($(this).val() == value.toString()) {
                        $(this).attr('checked', true);
                    }
                }
            });

            $('input[type=checkbox]').each(function () {
                var field = $(this).attr('name');
                var value = $(data).attr(field);

                if (field && value) {
                    if ($(this).val() == value.toString()) {
                        $(this).attr('checked', true);
                    }
                }
            });
        }
    }

    function showView(view) {
        $('.view').each(function () {
            if ($(this).is(':visible') && $(this).data('view') != view) {
                $(this).fadeOut();
            }
        });

        $('.view[data-view=' + view + ']').fadeIn();
    }

    var query = function (name) {
        return decodeURIComponent((new RegExp('[?|&]' + name + '=' + '([^&;]+?)(&|#|;|$)').exec(location.search) || [undefined, ""])[1].replace(/\+/g, '%20')) || null;
    };

    return {
        init: init,
        showSuccessMessage: showSuccessMessage,
        showInfoMessage: showInfoMessage,
        showWarningMessage: showWarningMessage,
        showDangerMessage: showDangerMessage,
        callService: callService,
        serviceGet: serviceGet,
        servicePost: servicePost,
        template: template,
        setAjaxSubmit: setAjaxSubmit,
        fillForm: fillForm,
        query: query,
        showView: showView
    };
})();

Mandril.init();