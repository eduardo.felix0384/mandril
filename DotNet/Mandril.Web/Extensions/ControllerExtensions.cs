using Mandril.Web.MVC;

namespace System.Web.Mvc
{
    public static class ControllerExtensions
    {
        public static ApiResult ApiOk(this Controller controller)
        {
            return new ApiResult();
        }

        public static ApiResult ApiOk(this Controller controller, object model)
        {
            return new ApiResult(model);
        }

        public static ApiResult ApiError(this Controller controller, Exception ex)
        {
            return new ApiResult(ex);
        }

        public static ApiResult ApiError(this Controller controller, int code, string message)
        {
            return new ApiResult(new
            {
                error = new ApiErrorModel()
                {
                    code = code,
                    message = message
                }
            });
        }
    }
}
