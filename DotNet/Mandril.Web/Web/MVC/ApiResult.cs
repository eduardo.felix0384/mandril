using System;
using System.Collections.Generic;
using System.Web;
using System.Web.Mvc;

namespace Mandril.Web.MVC
{
    public class ApiResult : ActionResult
    {
        readonly int type;

        protected object Model { get; set; }
        protected Exception Exception { get; set; } 
        protected string Error { get; set; }
        public IEnumerable<string> Errors { get; set; }

        public ApiResult()
        {
            type = 0;
        }

        public ApiResult(Exception ex)
        {
            type = 2;
            this.Exception = ex;
        }

        public ApiResult(object model)
        {
            type = 1;
            this.Model = model;
        }

        public override void ExecuteResult(ControllerContext context)
        {
            string result = null;

            switch(type)
            {
                case 0:
                    result = "{}";
                    break;

                case 1:
                    result = this.Model.JsonSerialize();
                    break;

                case 2:
                    result = new { error = new { code = -1, message = this.Exception.GetFullDescription() } }.JsonSerialize();
                    break;
            }

            HttpContext.Current.Response.ContentType = "application/json";
            HttpContext.Current.Response.Write(result);
        }
    }
}
