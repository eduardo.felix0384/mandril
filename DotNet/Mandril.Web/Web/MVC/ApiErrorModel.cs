﻿namespace Mandril.Web.MVC
{
    internal class ApiErrorModel
    {
        public int code { get; set; }
        public string message { get; set; }
    }
}
