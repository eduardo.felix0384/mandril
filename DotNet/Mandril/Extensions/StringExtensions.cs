using System.Globalization;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web.Script.Serialization;

namespace System
{
    public static class StringExtensions
    {
        public static bool IsNullOrEmpty(this string text)
        {
            return String.IsNullOrEmpty(text);
        }

        public static string Slugify(this string phrase)
        {
            string str = phrase.RemoveAccent().ToLower();
            str = Regex.Replace(str, @"[^a-z0-9\s-]", "");
            str = Regex.Replace(str, @"\s+", " ").Trim();
            str = str.Substring(0, str.Length <= 45 ? str.Length : 45).Trim();
            str = Regex.Replace(str, @"\s", "-");
            return str;
        }

        public static string RemoveAccent(this string txt)
        {
            byte[] bytes = System.Text.Encoding.GetEncoding("Cyrillic").GetBytes(txt);
            return System.Text.Encoding.ASCII.GetString(bytes);
        }

        public static bool IsStrongerPassword(this string password, string login)
        {
            var length = password.Length;
            bool hasDigit = Regex.Match(password, @"\d+").Success;
            bool hasLowerLetter = Regex.Match(password, @"[a-z]").Success;
            bool hasUpperLetter = Regex.Match(password, @"[A-Z]").Success;
            bool hasOthers = Regex.Match(password, @"[!,@,#,$,%,^,&,*,?,_,~,-,£,(,)]").Success;
            bool noLogin = password.IndexOf(login, StringComparison.InvariantCultureIgnoreCase) == -1;
            bool notSequencial = true;


            int count1L = 0;
            int count2L = 0;

            int count1N = 0;
            int count2N = 0;
            int count3N = 0;

            char? previous = null;
            foreach (char c in password)
            {
                if (previous.HasValue)
                {
                    if (char.IsLetter(c))
                    {
                        count1N = 0;
                        count2N = 0;
                        count3N = 0;

                        if (char.IsLetter(previous.Value))
                        {
                            if ((int)c == ((int)previous.Value) + 1)
                            {
                                count1L++;

                                if (count1L >= 2)
                                {
                                    notSequencial = false;
                                    break;
                                }
                            }
                            else
                                count1L = 0;

                            if ((int)c == ((int)previous.Value))
                            {
                                count2L++;

                                if (count2L >= 2)
                                {
                                    notSequencial = false;
                                    break;
                                }
                            }
                            else
                                count2L = 0;
                        }
                        else
                        {
                            count1L = 0;
                            count2L = 0;
                        }
                    }
                    else if (char.IsNumber(c))
                    {
                        count1L = 0;
                        count2L = 0;

                        if (char.IsNumber(previous.Value))
                        {
                            int n1 = int.Parse(c.ToString());
                            int n2 = int.Parse(previous.Value.ToString());

                            if (n1 == n2 + 1)
                            {
                                count1N++;

                                if (count1N >= 2)
                                {
                                    notSequencial = false;
                                    break;
                                }
                            }
                            else
                                count1N = 0;

                            if (n1 == n2 - 1)
                            {
                                count2N++;

                                if (count2N >= 2)
                                {
                                    notSequencial = false;
                                    break;
                                }
                            }
                            else
                                count2N = 0;

                            if (n1 == n2)
                            {
                                count3N++;

                                if (count3N >= 2)
                                {
                                    notSequencial = false;
                                    break;
                                }
                            }
                            else
                                count3N = 0;
                        }
                        else
                        {
                            count1N = 0;
                            count2N = 0;
                            count3N = 0;
                        }
                    }
                }

                previous = c;
            }

            return length > 8 &&
                    hasDigit &&
                    hasLowerLetter &&
                    hasUpperLetter &&
                    hasOthers &&
                    noLogin &&
                    notSequencial;
        }

        public static int[] ToIntArray(this string text)
        {
            try
            {
                return text.Split(",".ToCharArray(), StringSplitOptions.RemoveEmptyEntries).Select(i => int.Parse(i)).ToArray();
            }
            catch
            {
                return null;
            }
        }

        public static T JsonDeserialize<T>(this string json)
        {
            return new JavaScriptSerializer().Deserialize<T>(json);
        }

        public static DateTime ToDateTime(this string text, string culture)
        {
            return DateTime.Parse(text, new CultureInfo(culture));
        }

        public static DateTime ToDateTime(this string text)
        {
            return text.ToDateTime("pt-BR");
        }

        public static Guid ToGuid(this string guid)
        {
            return new Guid(guid);
        }
    }
}
