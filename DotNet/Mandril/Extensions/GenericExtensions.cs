using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Script.Serialization;

namespace System
{
    public static class GenericExtensions
    {
        private static Random random = new Random();

        public static string SaveAndGetFileName(this HttpPostedFileBase file, string path)
        {
            string extension = Path.GetExtension(file.FileName);
            var realFileName = Path.GetFileName(file.FileName);
            string fileName = $"{realFileName.Replace(extension, String.Empty)}_{DateTime.Now.ToString("yyyyMMddHHmmss")}{extension}";

            if (path[path.Length - 1] != '\\')
                path = $@"{path}\";

            var fullPath = $"{path}{fileName}";
            file.SaveAs(fullPath);

            return fileName;
        }

        public static string JsonSerialize(this object obj)
        {
            return new JavaScriptSerializer().Serialize(obj);
        }
        
        public static void Shuffle<T>(this IList<T> list)
        {
            int n = list.Count;
            while (n > 1)
            {
                n--;
                int k = random.Next(n + 1);
                T value = list[k];
                list[k] = list[n];
                list[n] = value;
            }
        }

        public static TTarget CopyDataTo<TTarget>(this object obj, ref TTarget target)
        {
            var objProperties = obj.GetType().GetProperties();
            var modelProperties = target.GetType().GetProperties();

            foreach (var mProp in modelProperties)
            {
                var name = mProp.Name.Replace("_", String.Empty).ToLower();
                var oProp = objProperties.FirstOrDefault(p => p.Name.Replace("_", String.Empty).ToLower() == name);

                if (oProp != null)
                {
                    try
                    {
                        var val = oProp.GetValue(obj, null);

                        if (val != null)
                            mProp.SetValue(target, val, null);
                    }
                    catch { }
                }
            }

            return target;
        }

        public static string ToCommaString(IEnumerable<int> array)
        {
            return String.Join(",", array.Select(i => i.ToString()));
        }

        public static string GetFullDescription(this Exception exception)
        {
            return String.Format("ErrorMessage: {0} | StackTrace: {1} | Inner ErrorMessage: {2} | Inner StackTrace: {3}", exception.Message, exception.StackTrace, (exception.InnerException != null) ? exception.InnerException.Message : String.Empty, (exception.InnerException != null) ? exception.InnerException.StackTrace : String.Empty).Trim();
        }
    }
}
