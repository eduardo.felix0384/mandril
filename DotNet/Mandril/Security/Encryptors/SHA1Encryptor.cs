using System;
using System.Security.Cryptography;
using System.Text;

namespace Mandril.Security.Encryptors
{
    internal class SHA1Encryptor : EncryptorBase
    {
        public override string Decrypt(string text)
        {
            return null;
        }

        public override string Encrypt(string text)
        {
            var sha = new SHA1CryptoServiceProvider();

            var originalBytes = ASCIIEncoding.Default.GetBytes(text);
            var encodedBytes = sha.ComputeHash(originalBytes);

            return BitConverter.ToString(encodedBytes);
        }
    }
}
