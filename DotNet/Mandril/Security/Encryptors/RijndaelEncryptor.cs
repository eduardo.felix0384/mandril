using System;
using System.IO;
using System.Security.Cryptography;
using System.Text;

namespace Mandril.Security.Encryptors
{
    class RijndaelEncryptor : EncryptorBase
    {
        public override string Decrypt(string text)
        {
            try
            {
                var RijndaelCipher = new RijndaelManaged();
                var EncryptedData = Convert.FromBase64String(text);
                var Salt = Encoding.ASCII.GetBytes(this.EncryptKey.Length.ToString());
                var SecretKey = new PasswordDeriveBytes(this.EncryptKey, Salt);
                var Decryptor = RijndaelCipher.CreateDecryptor(SecretKey.GetBytes(32), SecretKey.GetBytes(16));
                var memoryStream = new MemoryStream(EncryptedData);
                var cryptoStream = new CryptoStream(memoryStream, Decryptor, CryptoStreamMode.Read);
                var PlainText = new byte[EncryptedData.Length];
                var DecryptedCount = cryptoStream.Read(PlainText, 0, PlainText.Length);

                memoryStream.Close();
                cryptoStream.Close();

                var DecryptedData = Encoding.Unicode.GetString(PlainText, 0, DecryptedCount);

                return DecryptedData;
            }
            catch
            {
                throw new ApplicationException("Erro ao descriptografar a senha");
            }
        }

        public override string Encrypt(string text)
        {
            var RijndaelCipher = new RijndaelManaged();
            var PlainText = System.Text.Encoding.Unicode.GetBytes(text);
            var Salt = Encoding.ASCII.GetBytes(this.EncryptKey.Length.ToString());
            var SecretKey = new PasswordDeriveBytes(this.EncryptKey, Salt);
            var Encryptor = RijndaelCipher.CreateEncryptor(SecretKey.GetBytes(32), SecretKey.GetBytes(16));
            var memoryStream = new MemoryStream();
            var cryptoStream = new CryptoStream(memoryStream, Encryptor, CryptoStreamMode.Write);

            cryptoStream.Write(PlainText, 0, PlainText.Length);
            cryptoStream.FlushFinalBlock();

            var CipherBytes = memoryStream.ToArray();

            memoryStream.Close();
            cryptoStream.Close();

            var EncryptedData = Convert.ToBase64String(CipherBytes);

            return EncryptedData;
        }
    }
}
