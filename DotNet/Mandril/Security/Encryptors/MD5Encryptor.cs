using System;
using System.Security.Cryptography;
using System.Text;

namespace Mandril.Security.Encryptors
{
    internal class MD5Encryptor : EncryptorBase
    {
        public override string Decrypt(string text)
        {
            return null;
        }

        public override string Encrypt(string text)
        {
            var md  = new MD5CryptoServiceProvider();

            var originalBytes = ASCIIEncoding.Default.GetBytes(text);
            var encodedBytes = md.ComputeHash(originalBytes);

            return BitConverter.ToString(encodedBytes);
        }
    }
}
