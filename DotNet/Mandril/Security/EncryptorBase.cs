using Mandril.Configuration;

namespace Mandril.Security
{
    abstract class EncryptorBase
    {
        protected string EncryptKey
        {
            get
            {
                return MandrilConfig.Current.EncryptKey;
            }
        }

        public abstract string Encrypt(string text);
        public abstract string Decrypt(string text);
    }
}
