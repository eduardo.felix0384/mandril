using Mandril.Configuration;
using Mandril.Security.Encryptors;

namespace Mandril.Security
{
    class EncryptorFactory
    {
        protected EncryptorFactory() { }

        public static EncryptorBase GetEncryptor()
        {
            switch(MandrilConfig.Current.EncryptAlgorithm)
            {
                case MandrilConfig.eType.Rijndael:
                    return new RijndaelEncryptor();
                case MandrilConfig.eType.SHA1:
                    return new SHA1Encryptor();
                case MandrilConfig.eType.MD5:
                    return new MD5Encryptor();
                default:
                    return new RijndaelEncryptor();
            }
        }
    }
}
