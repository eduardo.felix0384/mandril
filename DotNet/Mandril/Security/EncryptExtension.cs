using Mandril.Security;

namespace System
{
    public static class EncryptExtension
    {
        public static string Encrypt(this string text)
        {
            if (text.IsNullOrEmpty())
                return text;

            return EncryptorFactory.GetEncryptor().Encrypt(text);
        }

        public static string Decrypt(this string text)
        {
            if (text.IsNullOrEmpty())
                return text;

            return EncryptorFactory.GetEncryptor().Decrypt(text);
        }
    }
}
