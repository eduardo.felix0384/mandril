using System;

namespace Mandril.Models
{
    public interface ISoftDeleteModel
    {
        DateTime? DataExclusao { get; set; }
    }
}
