using System;

namespace Mandril.Models
{
    public interface IModel
    {
        DateTime DataInsercao { get; set; }
        DateTime? DataAtualizacao { get; set; }
    }
}
