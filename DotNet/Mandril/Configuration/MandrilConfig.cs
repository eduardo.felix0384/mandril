using System;
using System.Configuration;
using System.Web;

namespace Mandril.Configuration
{
    public class MandrilConfig : ConfigurationSection
    {
        [ConfigurationProperty("defaultConnectionStringName", IsRequired = true)]
        public string DefaultConnectionStringName
        {
            get
            {
                if (this["defaultConnectionStringName"] == null)
                    return String.Empty;

                return this["defaultConnectionStringName"].ToString();
            }
            set
            {
                this["defaultConnectionStringName"] = value;
            }
        }

        [ConfigurationProperty("baseUrl", IsRequired = false)]
        public string BaseUrl
        {
            get
            {
                if (this["baseUrl"] == null)
                    return String.Empty;

                var result = this["baseUrl"].ToString();

                if (!result.IsNullOrEmpty() && result[result.Length - 1] == '/')
                    result = result.Substring(0, result.Length - 1);

                return result;
            }
            set
            {
                this["baseUrl"] = value;
            }
        }

        [ConfigurationProperty("cdnUrl", IsRequired = false)]
        public string CdnUrl
        {
            get
            {
                if (this["cdnUrl"] == null)
                    return String.Empty;

                var result = this["cdnUrl"].ToString();

                if (!result.IsNullOrEmpty() && result[result.Length - 1] == '/')
                    result = result.Substring(0, result.Length - 1);

                return result;
            }
            set
            {
                this["cdnUrl"] = value;
            }
        }

        public string SiteUrl
        {
            get
            {
                var url = HttpContext.Current.Request.Url;
                return String.Format("{0}://{1}{2}{3}", url.Scheme, url.Host, (url.Port != 80) ? String.Format(":{0}", url.Port) : String.Empty, BaseUrl);
            }
        }

        [ConfigurationProperty("tablePrefix", IsRequired = true)]
        public string TablePrefix
        {
            get
            {
                if (this["tablePrefix"] == null)
                    return String.Empty;

                return this["tablePrefix"].ToString();
            }
            set
            {
                this["tablePrefix"] = value;
            }
        }

        [ConfigurationProperty("encryptKey", IsRequired = false)]
        public string EncryptKey
        {
            get
            {
                if (this["encryptKey"] == null)
                    return String.Empty;

                return this["encryptKey"].ToString();
            }
            set
            {
                this["encryptKey"] = value;
            }
        }

        public enum eType
        {
            Rijndael = 0,
            SHA1 = 1,
            MD5 = 2,
        }

        [ConfigurationProperty("encryptAlgorithm", IsRequired = true, IsKey = true)]
        public eType EncryptAlgorithm
        {
            get
            {
                if (this["encryptAlgorithm"] == null)
                    return eType.Rijndael;

                return (eType)this["encryptAlgorithm"];
            }
            set
            {
                this["encryptAlgorithm"] = value;
            }
        }

        public static MandrilConfig Current
        {
            get
            {
                return ConfigurationManager.GetSection("mandrilConfigurations") as MandrilConfig;
            }
        }
    }
}
